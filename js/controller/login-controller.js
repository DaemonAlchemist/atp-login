/**
 * Created by Andy on 8/22/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";

export default function loginController() {
    //Redirect all routes to the login page if not logged in
    router.addRoute(/^.*$/, () => {
        console.log("Login check controller");
        if (!rest.isLoggedIn() && location.hash != "#login") {
            console.log("User not logged in.  Redirecting to login page");
            router.run("login");
        }
    });

    //Show the login form
    router.addRoute(/^login$/, () => {
        console.log("Login controller");
        if(rest.isLoggedIn()) {
            console.log("User already logged in.  Redirecting to homepage");
            router.run("");
            return;
        }
        load.template("module/login/views/login-form").then(form => {
            $("#page-content").html(form());
            $("#login-form").restForm()
                .then(data => {
                    console.log("User logged in");
                    $(document).trigger('atp.auth.login.success');
                    console.log("Login token: " + data.loginToken);
                    rest.setLoginToken(data.loginToken);
                    rest.call('profile').then((data) => {
                        $(document).trigger('atp.user.profile.load', [data]);
                        router.run("home");
                    });
                })
                .catch(() => {
                    $(document).trigger('atp.auth.login.error')
                });
        }).catch((e) => {
            $("#page-content").append("Could not load login form");
            throw e;
        });
    });

    router.addRoute(/^logout$/, () => {
        rest.setLoginToken("");
        setTimeout(() => router.run("login"), 100);
    });
}
