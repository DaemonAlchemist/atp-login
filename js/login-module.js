/**
 * Created by Andy on 8/20/2016.
 */

import loginController from "./controller/login-controller.js";
import dashboard from "../../core/js/dashboard.js";
import load from "../../core/js/loader.js";

export default function loginModule() {
    loginController();

    dashboard.addMenu("account", {
        title: "My Account",
        icon: "fa fa-user",
        url: "",
        location: 'right',
    });
    dashboard.addMenu("account:logout", {
        title: "Logout",
        icon: "fa fa-sign-out",
        url: "#logout",
    });

    dashboard.addWidget("testWidget", container => {
        load.template("module/login/views/test-widget").then(widget => {
            container.append(widget());
        })
    });
}